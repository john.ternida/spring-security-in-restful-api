package com.bootcamp.springsecurityinrestfulapi.Repository;

import com.bootcamp.springsecurityinrestfulapi.Entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course, Long> {
}
