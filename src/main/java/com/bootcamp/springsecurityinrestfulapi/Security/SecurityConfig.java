package com.bootcamp.springsecurityinrestfulapi.Security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true,proxyTargetClass=true)
public class SecurityConfig  extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication()
                .withUser("user").password("{noop}password").roles("USER")
                .and()
                .withUser("admin").password("{noop}password").roles("ADMIN");
    }

    protected void configure(HttpSecurity http) throws Exception{
        http
                .authorizeRequests()
                    .antMatchers(HttpMethod.POST, "/students/addStudent").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/students/addGpa").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/students/getAllStudents").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/students/getById").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.GET, "/students/deleteById").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PUT, "/students/updateById").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/students/{id}/enroll").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/students/{id}/drop").hasRole("ADMIN")

                    .antMatchers(HttpMethod.POST, "/courses/addCourse").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/courses/getAllCourses").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/courses/getCourseById").hasAnyRole("USER", "ADMIN")
                    .antMatchers(HttpMethod.GET, "/courses/deleteCourse").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PUT, "/courses/updateById").hasRole("ADMIN")
                    .anyRequest().authenticated()
                    .and()
                .httpBasic()
                    .and()
                    .csrf().disable();
    }






}
