package com.bootcamp.springsecurityinrestfulapi.Controller;

import com.bootcamp.springsecurityinrestfulapi.Entity.Course;
import com.bootcamp.springsecurityinrestfulapi.Repository.CourseRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/courses")
public class CourseController {


    private final CourseRepository courseRepository;

    public CourseController(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }


    @PostMapping("/addCourse")
    public Course addCourse(@RequestBody Course course){
        return courseRepository.save(course);
    }

    @GetMapping("/getAllCourses")
    public List<Course> getAllCourse(){
        return courseRepository.findAll();
    }

    @GetMapping("/getCourseById/{id}")
    public Optional<Course> getCourseById(@PathVariable Long id){
        return courseRepository.findById(id);
    }

    @GetMapping("/deleteCourse/{id}")
    public void deleteCourse(@PathVariable Long id){
        courseRepository.deleteById(id);
    }

    @PutMapping("/updateCourse/{id}")
    public Course updateCourse(@PathVariable Long id, @RequestBody Course updatedCourse){
        Course course = courseRepository.findById(id).orElseThrow();
        course.setName(updatedCourse.getName());
        course.setDescription(updatedCourse.getDescription());
        return courseRepository.save(course);

    }



}
