package com.bootcamp.springsecurityinrestfulapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityInRestfulApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityInRestfulApiApplication.class, args);
	}

}
