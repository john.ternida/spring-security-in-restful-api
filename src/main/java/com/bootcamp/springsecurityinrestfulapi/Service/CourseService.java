package com.bootcamp.springsecurityinrestfulapi.Service;

import com.bootcamp.springsecurityinrestfulapi.Entity.Course;
import com.bootcamp.springsecurityinrestfulapi.Repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Service
public class CourseService {

        private final CourseRepository courseRepository;

        @Autowired
        public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }




    public Course addCourse( Course course){
        return courseRepository.save(course);
    }


    public List<Course> getAllCourse(){
        return courseRepository.findAll();
    }


    public Optional<Course> getCourseById(Long id){
        return courseRepository.findById(id);
    }


    public void deleteCourse(Long id){
        courseRepository.deleteById(id);
    }


    public Course updateCourse( Long id,  Course updatedCourse){
        Course course = courseRepository.findById(id).orElseThrow();
        course.setName(updatedCourse.getName());
        course.setDescription(updatedCourse.getDescription());
        return courseRepository.save(course);

    }








}
