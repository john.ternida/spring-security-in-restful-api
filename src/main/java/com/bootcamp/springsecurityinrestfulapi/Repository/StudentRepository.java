package com.bootcamp.springsecurityinrestfulapi.Repository;

import com.bootcamp.springsecurityinrestfulapi.Entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
